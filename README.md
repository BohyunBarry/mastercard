# Jersey Store

Jersey Store® is a hip new business that specializes in the trade of soccer jerseys (how cool is that!).

You've been hired as a Java developer to create a RESTful service for their jersey stock management. 

Your task is to finish the work that the previous developer already started and get the API up and running. 


## Getting Started

You are free to use any library you want to get the job done.

### Prerequisites

* Java 8 
* Maven 3

### Installing


```
mvn clean package
```

execute

```
mvn spring-boot:run
```


## Goals

* Finish the CRUD API for the JerseyController.(DONE)
    * You're given 2 GET methods on the controller, you need to implement the [REST](https://media.giphy.com/media/SUeUCn53naadO/giphy.gif)...
* Fix the code on the File Loader.(DONE)
    * The previous developer did a really poor job writing the CSV loader - highlight his mistakes with comments in the current file.(DONE)
    * There's no point trying to save that awful code - could you implement a better solution in `GoodFileLoader`? (DONE)
    * [BONUS] There's been a report of a discrepancy between the warehouse stock (`warehouseStock.csv`) and the database. Can you find the missing jerseys?
	Answer: the "Away" should be Caps letter
* Write tests to ensure that the code is correct
* The SalesController currently uses a GET method to add a sale, and doesn't take an amount. Can you improve this method?(DONE)
* Enhance Jersey to include a `material` which defaults to `COTTON`. Values allowed should be `COTTON` or `NYLON`.(DONE)
* Fix any code-issues that you find (such as poorly scoped variables, etc.)(DONE)
* Ensure that the code compiles and can be easily run when reviewing. Include any necessary instructions.(DONE)

### Bonus

* **Security:** Some resources should be protected. Can you ensure that only the right users can delete data?
  * The user can be generated at runtime and doesn't have to be saved to the database(DID)
  * A large bonus here would be saving a user to the database, and taking the correct security precautions when doing so
* **Versioning:** Can you add a new controller to represent a version 2 without breaking the existing contracts?
  * Can you ensure that the original response is maintained without the additional `material` field?(DO it in another way)
* **Threading:** The SalesServiceTest is currently failing. The total sales don't match what is expected. Can you fix the issue?
  * The method can be rewritten as you see fit.(DONE)
