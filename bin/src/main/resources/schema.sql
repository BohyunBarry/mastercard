DROP TABLE IF EXISTS jersey;
CREATE TABLE IF NOT EXISTS jersey (
  id VARCHAR(36) PRIMARY KEY,
  size INT,
  brand VARCHAR(50),
  club VARCHAR(50),
  year YEAR,
  type INT,
  cut INT
);