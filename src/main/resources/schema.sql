DROP TABLE IF EXISTS jersey;
CREATE TABLE IF NOT EXISTS jersey (
  id VARCHAR(36) PRIMARY KEY,
  size INT,
  brand VARCHAR(50),
  club VARCHAR(50),
  year YEAR,
  type INT,
  cut INT,
  material INT
);

CREATE TABLE IF NOT EXISTS user (
	id VARCHAR(36) PRIMARY KEY,
    name varchar(50) not null,
    address varchar(120) not null,
    email varchar(120) not null,
    enabled boolean not null
);

