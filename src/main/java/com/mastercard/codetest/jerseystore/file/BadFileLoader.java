package com.mastercard.codetest.jerseystore.file;

import com.mastercard.codetest.jerseystore.model.Jersey;
import com.mastercard.codetest.jerseystore.model.JerseyCut;
import com.mastercard.codetest.jerseystore.model.JerseySize;
import com.mastercard.codetest.jerseystore.model.JerseyType;
import com.mastercard.codetest.jerseystore.service.JerseyStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;

/**
 * What's wrong with this implementation ?
 * Add comments around the issues you identify and suggest ways to improve the code
 */
@Component
public class BadFileLoader extends FileLoader {

    private JerseyStoreService jerseyStoreService;

    @Autowired
    public BadFileLoader(JerseyStoreService jerseyStoreService){
        this.jerseyStoreService = jerseyStoreService;
    }

    public void load(String filename) {
        try {
            File f = new File(filename);
            FileReader r = new FileReader(f);
            int i;
            String fileContent = "";
            while ((i = r.read()) != -1) {
                System.out.print((char) i);
                fileContent += (char) i;
            }
            String[] split = fileContent.split(",");
            for(int j = 0; j < split.length; j += 7) {
                String s = split[j].trim();
                String b = split[j + 1];
                String cl = split[j + 2];
                String y = split[j + 3];
                String t = split[j + 4];
                String c = split[j + 5];
                String a = split[j + 6];
                
                /*
                 * This part didn't put all the elements in an object
                 * **/
                for(int k = 0; k < Integer.parseInt(a); k++) {
                    try {
                        Jersey jersey = new Jersey();
                        jersey.setSize(JerseySize.valueOf(s));
                        jersey.setBrand(b);
                        jersey.setClub(cl);
                        jersey.setYear(y);
                        jersey.setType(JerseyType.valueOf(t));
                        jersey.setCut(JerseyCut.valueOf(c));
                        jerseyStoreService.addJersey(jersey);
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
