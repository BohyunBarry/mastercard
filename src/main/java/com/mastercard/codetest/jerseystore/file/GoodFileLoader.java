package com.mastercard.codetest.jerseystore.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.codetest.jerseystore.model.Jersey;
import com.mastercard.codetest.jerseystore.model.JerseyCut;
import com.mastercard.codetest.jerseystore.model.JerseyMaterial;
import com.mastercard.codetest.jerseystore.model.JerseySize;
import com.mastercard.codetest.jerseystore.model.JerseyType;
import com.mastercard.codetest.jerseystore.service.JerseyStoreService;


@Component
public class GoodFileLoader extends FileLoader {

	private JerseyStoreService jerseyStoreService;

	@Autowired
	public GoodFileLoader(JerseyStoreService jerseyStoreService) {
		this.jerseyStoreService = jerseyStoreService;
	}

	public void load(String path) {
		// TODO: implement me
		try {
			File f = new File(path);
			FileInputStream fis = new FileInputStream(path);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			String line = null;

			while ((line = br.readLine()) != null) {
				System.out.println(line);
				StringTokenizer token = new StringTokenizer(line, ",");
				int tokenCount = token.countTokens();
				int i = 0;
				Jersey j = new Jersey();
				do {
					switch (i) {
					case 0:
						j.setSize(JerseySize.valueOf(token.nextToken()));
						break;
					case 1:
						j.setBrand(token.nextToken());
						break;
					case 2:
						j.setClub(token.nextToken());
						break;
					case 3:
						j.setYear(token.nextToken());
						break;
					case 4:
						j.setType(JerseyType.valueOf(token.nextToken().toUpperCase()));
						break;
					case 5:
						j.setCut(JerseyCut.valueOf(token.nextToken()));
						break;
					case 6:
						j.setMaterial(JerseyMaterial.valueOf(Integer.parseInt(token.nextToken())));
						break;
					default:
//						jerseyStoreService.addJersey(j);
						break;
					}
					++i;
					
				} while (token.hasMoreTokens() && i < tokenCount);
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
