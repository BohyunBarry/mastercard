package com.mastercard.codetest.jerseystore.model;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

import java.util.Map;

public enum JerseyMaterial {
	COTTON(1), NYLON(2);
    private final static Map<Integer, JerseyMaterial> map =
            stream(JerseyMaterial.values()).collect(toMap(e -> e.id, e -> e));
    
    private int id;

    JerseyMaterial(int id) {
        this.id = id;
    }

	public int getId() {
		return id;
	}
	
    public static JerseyMaterial valueOf(int id) {
        return map.get(id);
    }
    
}
