package com.mastercard.codetest.jerseystore.model;

public class Jersey {

    private String id;
    private JerseySize size;
    private String brand;
    private String club;
    private String year;
    private JerseyType type;
    private JerseyCut cut;
    private JerseyMaterial material;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JerseySize getSize() {
        return size;
    }

    public void setSize(JerseySize size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public JerseyType getType() {
        return type;
    }

    public void setType(JerseyType type) {
        this.type = type;
    }

    public JerseyCut getCut() {
        return cut;
    }

    public void setCut(JerseyCut cut) {
        this.cut = cut;
    }
    

    public JerseyMaterial getMaterial() {
		return material;
	}

	public void setMaterial(JerseyMaterial material) {
		this.material = material;
	}

	public Jersey() {
    }

    
    
}
