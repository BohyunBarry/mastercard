package com.mastercard.codetest.jerseystore.service;

import com.mastercard.codetest.jerseystore.model.Jersey;
import com.mastercard.codetest.jerseystore.model.JerseyCut;
import com.mastercard.codetest.jerseystore.model.JerseyMaterial;
import com.mastercard.codetest.jerseystore.model.JerseySize;
import com.mastercard.codetest.jerseystore.model.JerseyType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class JerseyStoreService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JerseyStoreService.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JerseyStoreService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Jersey getJersey(String tShirtId) {
        return jdbcTemplate.queryForObject("SELECT * FROM JERSEY WHERE id = ?",
                new Object[]{tShirtId},
                new RowMapper<Jersey>() {
                    @Nullable
                    @Override
                    public Jersey mapRow(ResultSet resultSet, int i) throws SQLException {
                        Jersey jersey = new Jersey();
                        jersey.setId(resultSet.getString("id"));
                        jersey.setBrand(resultSet.getString("brand"));
                        jersey.setSize(JerseySize.valueOf(resultSet.getInt("size")));
                        jersey.setClub(resultSet.getString("club"));
                        jersey.setYear(resultSet.getString("year"));
                        jersey.setType(JerseyType.valueOf(resultSet.getInt("type")));
                        jersey.setCut(JerseyCut.valueOf(resultSet.getInt("cut")));
                        
                        //Add material
                        jersey.setMaterial(JerseyMaterial.valueOf(resultSet.getInt("material")));
                        return jersey;
                    }
                });
    }

    /**
     *  ID, SIZE, BRAND, CLUB, YEAR, TYPE, CUT
     * @param jersey
     */
    public boolean addJersey(Jersey jersey) {
        return jdbcTemplate.update("INSERT INTO jersey(ID, SIZE, BRAND, CLUB, YEAR, TYPE, CUT, MATERIAL) VALUES (?,?,?,?,?,?,?,?)",
                UUID.randomUUID().toString(), jersey.getSize().getId(), jersey.getBrand(), jersey.getClub(),
                jersey.getYear(), jersey.getType().getId(), jersey.getCut().getId(),jersey.getMaterial().getId()) > 0;
    }

    public List<Jersey> getAllJerseys() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM JERSEY");
        List<Jersey> jerseys = new ArrayList<>(rows.size());
        for(Map row: rows) {
            Jersey jersey = new Jersey();
            jersey.setId((String)row.get("id"));
            jersey.setBrand((String)row.get("brand"));
            jersey.setSize(JerseySize.valueOf((Integer) row.get("size")));
            jersey.setClub((String) row.get("club"));
            jersey.setYear("" + row.get("year"));
            jersey.setType(JerseyType.valueOf((Integer) row.get("type")));
            jersey.setCut(JerseyCut.valueOf((Integer) row.get("cut")));
            
            //Add Material
            jersey.setMaterial(JerseyMaterial.valueOf((Integer) row.get("material")));
            jerseys.add(jersey);
        }
        return jerseys;
    }  
}
