package com.mastercard.codetest.jerseystore.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class SalesService {

	public Logger logger = LoggerFactory.getLogger(JerseyStoreService.class);

	private int totalSales = 0;

	/**
	 * Adds a integer amount to the total sales, and returns the total sales
	 *
	 * @param saleAmount The integer amount to add to the total sales
	 *
	 * @return The total sales amount so far
	 */
	public int addSale(int saleAmount) {
		final int currentSales = totalSales;
		try {
			TimeUnit.MICROSECONDS.sleep(10);
		} catch (InterruptedException e) {
			logger.error("Interrupted while waiting for sales processing.", e);
		}
		return totalSales = currentSales + saleAmount;
	}

	public int getTotalSales() {
		return totalSales;
	}

}
