package com.mastercard.codetest.jerseystore.controller;

import com.mastercard.codetest.jerseystore.model.Jersey;
import com.mastercard.codetest.jerseystore.service.JerseyStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class JerseyController {

    private JerseyStoreService jerseyStoreService;

    @Autowired
    public JerseyController(JerseyStoreService jerseyStoreService) {
        this.jerseyStoreService = jerseyStoreService;
    }

    @GetMapping("/rest/api/v1/jersey/{id}")
    public Jersey getJersey(@PathVariable String id) {
        return jerseyStoreService.getJersey(id);
    }
    
    
    @GetMapping("/rest/api/v1/jersey")
    public List<Jersey> getJerseys() {
        return jerseyStoreService.getAllJerseys();
    }
    
    //AddJersey
    @GetMapping("/rest/api/v1/addJersey/{jersey}")
    public boolean addJersey(@PathVariable Jersey jersey) {
    	return jerseyStoreService.addJersey(jersey);
    }
}
