package com.mastercard.codetest.jerseystore.controller;

import com.mastercard.codetest.jerseystore.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SalesController {

	private final SalesService salesService;

	@Autowired
	public SalesController(SalesService salesService) {
		this.salesService = salesService;
	}

//	@GetMapping(value = "/rest/api/v1/sale")
//	public ResponseEntity<Integer> makeSale() {
//		final int totalSales = salesService.addSale(100);
//		return new ResponseEntity<>(totalSales, HttpStatus.CREATED);
//	}
	
	@GetMapping("/rest/api/v1/sale/{saleAmount}")
	public ResponseEntity<Integer> makeSale(@PathVariable int saleAmount){
		final int totalSales = salesService.addSale(saleAmount);
		return new ResponseEntity<>(totalSales,HttpStatus.CREATED);
	}

}
